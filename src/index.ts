import yargs from 'yargs';
import { addNote, getAllNotes, getNote, deleteNote, modifyNote } from './notes';

const { _, title, body, all } = yargs.argv;

const [command] = _;

switch (command) {
  case 'get': {
    if (title) {
      console.log(JSON.stringify(getNote(title), null, 2));
      break;
    } else if (all) {
      console.log(JSON.stringify(getAllNotes(), null, 2));
      break;
    }
  }
  case 'add': {
    addNote(title, body);
    break;
  }
  case 'delete': {
    deleteNote(title);
    break;
  }
  case 'modify': {
    modifyNote(title, body);
    break;
  }
  default: {
    console.log('Error');
    break;
  }
}
