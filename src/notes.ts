import fs from 'fs';
import path from 'path';

export interface Note {
  title: string;
  body: string;
  createdAt: Date;
  lastModifiedAt: Date;
}
const notePath = path.resolve(__dirname, './notes.json');

export const addNote = (title: string, body: string) => {
  const createdAt = new Date();
  const lastModifiedAt = createdAt;
  const note: Note = { title, body, createdAt, lastModifiedAt };
  const notesString = fs.readFileSync(notePath).toString('utf8');
  const notes: Note[] = JSON.parse(notesString);
  console.log(` Added a new note : \n${JSON.stringify(note)}`);
  notes.push(note);
  fs.writeFileSync(notePath, JSON.stringify(notes));
};

export const getAllNotes = (): Note[] => {
  const notesString = fs.readFileSync(notePath).toString('utf8');
  const notes: Note[] = JSON.parse(notesString);
  return notes;
};

export const getNote = (title: string) => {
  const notesString = fs.readFileSync(notePath).toString('utf8');
  const notes: Note[] = JSON.parse(notesString);
  const [note] = notes.filter(note => note.title === title);
  return note;
};

export const deleteNote = (title: string) => {
  const notesString = fs.readFileSync(notePath).toString('utf8');
  const notes: Note[] = JSON.parse(notesString);
  const [note] = notes.filter(note => note.title === title);
  console.log(` Deleted a note : \n${JSON.stringify(note)}`);
  notes.splice(notes.indexOf(note), 1);
  fs.writeFileSync(notePath, JSON.stringify(notes));
};

export const modifyNote = (title: string, body: string) => {
  const notesString = fs.readFileSync(notePath).toString('utf8');
  const notes: Note[] = JSON.parse(notesString);
  const [note] = notes.filter(note => note.title === title);
  const newNote: Note = {
    title: note.title,
    body: body,
    createdAt: note.createdAt,
    lastModifiedAt: new Date()
  };
  notes.splice(notes.indexOf(note), 1);
  console.log(` Modified a note : \n${JSON.stringify(newNote)}`);
  notes.push(newNote);
  fs.writeFileSync(notePath, JSON.stringify(notes));
};
